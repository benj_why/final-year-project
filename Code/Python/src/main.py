
import os, sys, inspect, thread, time

src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
arch_dir = os.path.abspath(os.path.join(src_dir, '../lib'))
sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))

import thread
import Leap
import Tkinter as tk
from Leap import KeyTapGesture
from playsound import playsound



class SampleListener(Leap.Listener):
    display_window = tk.Tk()


    def on_init(self, controller):
        print "Program Initialized"
        self.display()
        

    def on_connect(self, controller):
        print "Device Connected"

        # Enable gestures
        controller.enable_gesture(Leap.Gesture.TYPE_KEY_TAP)

    def on_disconnect(self, controller):
        print "Disconnected"

    def on_exit(self, controller):
        print "Exited"

    def on_frame(self, controller):
        frame = controller.frame() #Most recent frame in list of frames

        #Output frame information
        print "Frame id: %d, timestamp: %d, hands: %d, gestures: %d" % (
              frame.id, frame.timestamp, len(frame.hands), len(frame.gestures()))


        # Get gestures
        for gesture in frame.gestures():
            print "Gesture: %s" % (gesture.type)
            #This checks which gesture was made, and then checks which hand made the gesture
            if gesture.type is Leap.Gesture.TYPE_KEY_TAP:
                #Check if the left-most hand in frame is a left or right hand
                if gesture.hands.leftmost.is_left:
                    #Triggers the sound and UI 
                    thread.start_new_thread(playsound, ("../audio/piano-mp3_G5.mp3",))
                    self.hand_text.set("Hand: left")
                    self.note_text.set("Note: A1")
                    self.canvas.itemconfig(self.left_key, fill='black')
                    time.sleep(0.1)
                    self.canvas.itemconfig(self.left_key, fill='white')

                else:
                    thread.start_new_thread(playsound, ("../audio/piano-mp3_A1.mp3",))
                    self.hand_text.set("Hand: right")
                    self.note_text.set("Note: G5")
                    self.canvas.itemconfig(self.right_key, fill='black')
                    time.sleep(0.1)
                    self.canvas.itemconfig(self.right_key, fill='white')

    def display(self):
        #Initialise Display
        self.display_window.geometry("500x500")

        self.canvas = tk.Canvas(self.display_window, width = 450, height = 450)
        self.canvas.pack()   

        self.left_key = self.canvas.create_rectangle(10, 10, 225, 400, fill='white')
        self.right_key = self.canvas.create_rectangle(225, 10, 450, 400, fill='white')

        self.hand_text = tk.StringVar()
        self.hand_text.set("Hand:")
        hand_label = tk.Label(textvariable = self.hand_text)
        hand_label.pack()

        self.note_text = tk.StringVar()
        self.note_text.set("Note:")
        note_label = tk.Label(textvariable = self.note_text)
        note_label.pack()

    def keyFlash(self, key):
        self.canvas.itemconfig(key, fill='black')
        time.sleep(0.1)
        self.canvas.itemconfig(key, fill='white')

        


def main():
    # Create a sample listener and controller
    listener = SampleListener()
    controller = Leap.Controller()

    # Link the two so the controller receives events from listener
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print "Press Enter to quit..."
    try:
        listener.display_window.mainloop()
        sys.stdin.readline() #Outputs the tracking information as frames
    except KeyboardInterrupt:
        #Exits the program when enter is pressed
        listener.display_window.destroy()
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)


if __name__ == "__main__":
    main()
