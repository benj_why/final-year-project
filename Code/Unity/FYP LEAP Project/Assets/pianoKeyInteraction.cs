using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pianoKeyInteraction : MonoBehaviour
{
    public AudioSource audioSource;
    public GameObject slider;
    private float baseVolume;
    private float basePitch;

    // Start is called before the first frame update
    void Start()
    {
        baseVolume = 0.25f;
        basePitch = audioSource.pitch;
    }

    // Update is called once per frame
    void Update()
    {
        //Changes the key to red if the related audiosource is playing
        GetComponent<Renderer>().material.color = Color.white;
        if (audioSource.isPlaying)
        {
            GetComponent<Renderer>().material.color = Color.red;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        //Pitch of audiosource is set to the value of the pitch slider
        float sliderValue = slider.GetComponent<Leap.Unity.Interaction.InteractionSlider>().HorizontalSliderValue;
        audioSource.pitch = sliderValue;
        //Volume of audiosource is multiplied by the velocity of the collision
        audioSource.volume = baseVolume * (collision.relativeVelocity.magnitude);
        audioSource.Play();
        Debug.Log(name);

        //Base volume, magnitude of collision velocity, and modulated volume, are logged
        Debug.Log("base volume:" + baseVolume);
        Debug.Log("magnitude:" + collision.relativeVelocity.magnitude);
        Debug.Log("volume:" + audioSource.volume);

        //Pitch is logged
        Debug.Log("slider value:" + sliderValue);
        Debug.Log("pitch:" + audioSource.pitch);
    }
}

