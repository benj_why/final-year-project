using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pitchSlider : MonoBehaviour
{
    public GameObject slider;
    public AudioSource audioSource;
    private float basePosition;

    // Start is called before the first frame update
    void Start()
    {
        audioSource.pitch = 1;
        basePosition = slider.transform.position[2];
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(slider.transform.position);
        audioSource.pitch = (1 + (Mathf.Round(slider.transform.position[2] * 10) *0.1f) * 4);
    }
}
